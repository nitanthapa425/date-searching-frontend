import { Route, Routes } from "react-router-dom";
import Navbar from "./Component/Navbar";
import CreateOrder from "./Component/CreateOrder";
import ReadOrder from "./Component/ReadOrder";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" element={<ReadOrder></ReadOrder>}></Route>
        <Route path="/create" element={<CreateOrder></CreateOrder>}></Route>
        <Route path="*" element={<div> 404 Error finding page</div>}></Route>
      </Routes>
    </div>
  );
}

export default App;
