
import React from "react";
import { NavLink } from "react-router-dom";
import "./Navbar.css";

const Navbar = () => {
  return (
    <header className="navbar">
      <ul
        className="nav-list"
        style={{ border: "1px solid black", padding: "30px" }}
      >
        <li>
          <NavLink className="order1" to="/create">
            Create Order
          </NavLink>
        </li>
        <li>
          <NavLink className="order1" to="/">
            Read Order
          </NavLink>
        </li>
      </ul>
    </header>
  );
};

export default Navbar;
