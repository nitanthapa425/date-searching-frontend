import React from "react";
import { Table, Space } from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons"; // Import the icons

const ReadOrder = () => {
  const columns = [
    { title: "Name", dataIndex: "name", key: "name" },
    { title: "Email", dataIndex: "email", key: "email" },
    { title: "Food", dataIndex: "food", key: "food" },
    { title: "Date", dataIndex: "date", key: "date" },
    { title: "Price", dataIndex: "price", key: "price" },
    {
      title: "Actions",
      dataIndex: "actions",
      key: "actions",
      render: (text, record) => (
        <Space size="middle">
          <a style={{ color: "green" }}>
            <EditOutlined />
          </a>
          <a style={{ color: "red" }}>
            <DeleteOutlined />
          </a>
        </Space>
      ),
    },
  ];

  const data = [
    {
      key: "1",
      name: "John Doe",
      email: "john@example.com",
      food: "Pizza",
      date: "2023-10-11",
      price: "$12.99",
    },
    {
      key: "2",
      name: "Jane Smith",
      email: "jane@example.com",
      food: "Burger",
      date: "2023-10-12",
      price: "$9.99",
    },
  ];

  return (
    <div className="table-container">
      <Table columns={columns} dataSource={data} />
    </div>
  );
};

export default ReadOrder;
