import React from "react";
import { Form, Formik } from "formik";
import * as yup from "yup";
import FormikSelect from "./FormikSelect";
import FormikInput from "./FormikInput";
import { Input, Button, DatePicker } from "antd";
import "./Navbar.css"
const FormikForm = () => {
  let initialValues = {
    email: "",
    name: "",
    food: "",
    orderDate: "",
    price: 0,
  };
  let foodOptions = [
    { label: "Select", value: "", disabled: true },
    { label: "Pizza", value: "pizza" },
    { label: "MO:Mo", value: "momo" },
    { label: "Burger", value: "burger" },
    { label: "Biryani", value: "biryani" },
    { label: "Chicken Tikka", value: "chicken tikka" },
    { label: "Thakali khana", value: "thakali" },
  ];
  let onSubmit = (value, order) => {
    console.log(value);
  };
  let validationSchema = yup.object({
    email: yup
      .string()
      .required("Email is required")
      .matches(
        /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
        "match the email format"
      ),
    name: yup
      .string()
      .required("Full name is required ")
      .min(5, "Must be atleast 5 character")
      .max(15, "Must be less than 15 character")
      .matches(/^[a-zA-Z ]*$/, "only alphabet and space are allowed "),
    food: yup.string().required("Mention the food you want"),
    orderDate: yup.string().required("choose the order date"),
    price: yup
      .number()
      .required("Price is required")
      .min(100, "must be at least Rs 100"),
  });
  return (
    <div className="form-container">
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      {(formik) => {
        return (
          <Form>
            <div className="form-item">
              <label htmlFor="name">Name:</label>
              <FormikInput
                name="name"
                type="text"
                className="input-field"
                onChange={(e) => {
                  formik.setFieldValue("name", e.target.value);
                }}
                required={true}
              />
            </div>
            <div className="form-item">
              <label htmlFor="email">Email:</label>
              <FormikInput
                name="email"
                type="email"
                className="input-field"
                onChange={(e) => {
                  formik.setFieldValue("email", e.target.value);
                }}
                required={true}
              />
            </div>
            <div className="form-item">
              <label htmlFor="orderDate">Order Date:</label>
              <DatePicker
                name="orderDate"
                className="input-field"
                onChange={(value, dateString) => {
                  formik.setFieldValue("orderDate", dateString);
                }}
              />
            </div>
            <div className="form-item">
              <label htmlFor="food">Food:</label>
              <FormikSelect
                name="food"
                className="select-box"
                onChange={(e) => {
                  formik.setFieldValue("food", e.target.value);
                }}
                options={foodOptions}
                required={true}
              />
            </div>
            <div className="form-item">
              <label htmlFor="price">Price:</label>
              <FormikInput
                name="price"
                type="number"
                className="input-field"
                onChange={(e) => {
                  formik.setFieldValue("price", e.target.value);
                }}
                required={true}
              />
            </div>
            <div className="form-item">
              <button type="submit" className="submit-button">
                Create
              </button>
            </div>
          </Form>
        );
      }}
    </Formik>
  </div>
  
  );
};

export default FormikForm;
