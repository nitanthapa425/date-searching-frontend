import React from "react";
import FormikForm from "./FormikForm";

const CreateOrder = () => {
  return (
    <div>
      <FormikForm />
    </div>
  );
};

export default CreateOrder;
