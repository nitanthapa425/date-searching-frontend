import React from "react";
import { Field } from "formik";
const FormikSelect = ({
  name,
  label,
  type,
  onChange,
  required,
  options,
  ...props
}) => {
  return (
    <div>
      <Field name="country">
        {({ field, form, meta }) => {
          return (
            <div>
              <label htmlFor={name}>
                {label}{" "}
                {required ? <span style={{ color: "red" }}>*</span> : null}
              </label>
              <select
                {...field}
                {...props}
                id={name}
                value={meta.value}
                onChange={onChange ? onChange : field.onChange}
              >
                {options.map((item, i) => {
                  return (
                    <option key={i} value={item.value} disabled={item.disabled}>
                      {item.label}
                    </option>
                  );
                })}
                {/* <option value="nep">Nepal</option>
              <option value="ind">India</option>
              <option value="usa">USA</option>
              <option value="Chin">China</option> */}
              </select>
              {meta.touched && meta.error ? (
                <div style={{ color: "red" }}>{meta.error}</div>
              ) : null}
            </div>
          );
        }}
      </Field>
    </div>
  );
};

export default FormikSelect;
